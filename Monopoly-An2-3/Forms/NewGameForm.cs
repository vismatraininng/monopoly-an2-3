﻿using Monopoly_An2_3.Entities;
using Monopoly_An2_3.Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Monopoly_An2_3
{
    public partial class NewGameForm : Form
    {
        Game G = new Game();

        public NewGameForm()
        {
            InitializeComponent();
        }

        private void testButton_Click(object sender, EventArgs e)
        {
            foreach (var player in G.playerList)
            {
                if (player.Name.Trim() != string.Empty)
                {
                    Console.WriteLine("{0} - {1}", player.Name, player.Type);
                }
            }
            GameForm gameForm = new GameForm();
            gameForm.ShowDialog();
        }

        private void NewGameForm_Load(object sender, EventArgs e)
        {
            G.playerList.Add(new Player { Name = "", Money = 1000, Type = PlayerTypes.car });
            G.playerList.Add(new Player { Name = "", Money = 1000, Type = PlayerTypes.dog });
            G.playerList.Add(new Player { Name = "", Money = 1000, Type = PlayerTypes.hat });
            G.playerList.Add(new Player { Name = "", Money = 1000, Type = PlayerTypes.iron });
            G.playerList.Add(new Player { Name = "", Money = 1000, Type = PlayerTypes.shoe });
            G.playerList.Add(new Player { Name = "", Money = 1000, Type = PlayerTypes.thimble });
            G.playerList.Add(new Player { Name = "", Money = 1000, Type = PlayerTypes.wheelbarrow });

            playerGrid.DataSource = G.playerList;
        }
    }
}
